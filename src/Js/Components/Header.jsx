import React, { Component } from 'react'
import '../../Css/header.css'
import Logo from '../../Images/Logo-min.png'
export default class Header extends Component {
    render() {
        return (
            <header className="grid-header">
                <img src={Logo} alt="Logo" className="img-header"/>
                <h1 className="title-header">Los Reyes</h1>
            </header>
        );
    }
}
