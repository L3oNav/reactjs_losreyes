import React from 'react'
import Marca_logo from '../../Images/Ellos.jpg'
import '../../Css/Home.css'
import CervezasCard from '../Components/functions/cervezas.jsx'
export default class Home extends React.Component {
    render() {
        return (
            <div >
                {/* <h3>Hello world</h3> */}
                <div className="container">
                    <div alt="PadreEHijo">
                        <img src={Marca_logo} alt="Logo_grande" className="img-logo" />
                    </div>
                    <div className="div-somos">
                        <h1 className="title-home">Desde 2013</h1>
                        <p className="text-home">
                            Como parte del movimiento cervecero nace la primera cervecería de Playas de Rosarito por el año del 2013,
                            en manos de Luis E. Reyes padre y Luis E. Reyes hijo, siendo desde sus inicios una empresa familiar y hoy
                            más que nunca preocupada por el medio ambiente, contando con paneles solares, plata de agua, reúso de agua
                            para fines de limpieza general, uso del agua de condensación entre otros, teniendo como objeto ofrecer
                            productos de calidad, por medio de superación día a día. A través de los años hemos participado en diferentes
                            festivales del Estado, actualmente nos puedes encontrar en los mejores bares de especialidad del Estado.
                    </p>
                    </div>
                </div>
                <div>
                    <CervezasCard/>
                </div>
            </div>
        )
    }
}