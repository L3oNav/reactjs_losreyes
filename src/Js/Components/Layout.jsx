import React from 'react'
import Header from './Header'
import Navbar from './Navbar'
import Footer from './Footer'
import '../../Css/grid.css'
export default function Layout(props) {
    return(
        <div className="grid">
            <React.Fragment>
                <Header />
                <Navbar />
                <div className="grid-content">
                    {props.children}
                </div>
                <Footer />
            </React.Fragment>
        </div>
    );
}