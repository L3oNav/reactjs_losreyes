import React, { Component } from 'react'
import '../../Css/navbar.css'
// import Home from '../../Images/home.svg'
// import Beer from '../../Images/beer.svg'
// import images from '../../Images/images.svg'
// import contact from '../../Images/contact.svg'

import { Link } from 'react-router-dom'
export default class Navbar extends Component {
    render() {
        return (
            <nav className="grid-navbar">

                <div className="display-center-horizontal">

                    <Link to="/" className="grid-home">
                        <div className="navbar-display">
                            <div alt="IconoHome" className="nav-columns">
                                <i className="icon-home icon-size"></i>
                                <h3 className="text">Inicio</h3>
                            </div>
                        </div>
                    </Link>
                    {/* Boton de las cervezas */}
                    {/* <Link className="grid-beer">
                        <div className="navbar-display">
                            <div className="nav-columns" alt="IconBeer">
                                <i className="icon-beer icon-size"></i>
                                <h3 className="text">Cervezas</h3>
                            </div>
                        </div>
                    </Link> */}

                    <Link className="grid-images">
                        <div className="navbar-display">
                            <div className="nav-columns" alt="IconBeer">
                                <i className="icon-images icon-size"></i>
                                <h3 className="text">Multimedia</h3>
                            </div>
                        </div>
                    </Link>

                    <Link className="grid-contact">
                        <div className="navbar-display">
                            <div className="nav-columns" alt="IconBeer">
                                <i className="icon-mail icon-size"></i>
                                <h3 className="text">Contacto</h3>
                            </div>
                        </div>
                    </Link>
                </div>

            </nav>
        )
    }
}
