import React, { Component } from 'react'
import FilterBeers from './FilterBeers.jsx'


export default class CervezasCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Beers: [
                {
                    name: 'BROWN ALE',
                    tem:['Verano','Todas'],
                    alc: '5.4%',
                    ibu: '34%',
                    color: 'Cobrizo oscuro',
                    fragrance: 'Aromas tostados y chocolate',
                    taste: 'Maltas oscuras con notas tostadas',

                },
                {
                    name: 'PALE ALE',
                    tem:['Verano','Todas'],
                    alc: '5.5%',
                    ibu: '67%',
                    color: 'Amarillo Dorado',
                    fragrance: 'Cítricos florales',
                    taste: 'Intensa explosión de sabores amargos, florales y cítricos'
                },
                {
                    name: 'PALE ALE GENGIBRE',
                    tem:['Verano','Todas'],
                    alc: '5.5%',
                    ibu: '47%',
                    color: 'Amarillo Dorado',
                    fragrance: 'Jengibre',
                    taste: 'Cerveza amarga, presencia de jengibre y notas florales'
                },
                {
                    name: 'HEFEWEIZEN',
                    tem:['Verano','Todas'],
                    alc: '4%',
                    ibu: '30%',
                    color: 'Amarillo claro.',
                    fragrance: 'Trigo y cítricos.',
                    taste: 'Mezcla de sabores, trigo junto con un toque cítrico a cascara de naranja.'
                },
                {
                    name: 'PUMKING ALE',
                    tem:['Verano','Todas'],
                    alc: '7.5%',
                    ibu: '39%',
                    color: 'Naranja oscuro.',
                    taste: 'Explosión de sabores a especias, calabaza.',
                },
                {
                    name: 'BLONDE ALE',
                    alc: '4.5%',
                    tem:['Verano','Todas'],
                    ibu: '20%',
                    color: 'Claro dorado.',
                    fragrance: 'Maltas claras.',
                    taste: 'ligero sabor a malta.'
                },
                {
                    name: 'HONEY PORTER',
                    tem:['Navidad','Todas'],
                    alc: '8%',
                    ibu: '40%',
                    color: 'Negro.',
                    fragrance: 'Miel y maltas tostadas.',
                    taste: 'Maltas tostadas con notas dulces a miel.',
                },
                {
                    name: 'TRIPREL',
                    alc: '10%',
                    tem:['Navidad','Todas'],
                    ibu: '55%',
                    color: 'Claro dorado y un tanto turbia.',
                    fragrance: 'Fuertes aromas maltosos y frutales.',
                    taste: 'Fuerte presencia de malta, dulce y un tanto alcohólico.La champaña de la casa.',
                }
            ],
            Buttons:[
                {
                    id:'Todas',
                    txt:'Todas las cervezas disponibles.',
                },
                {
                    id:'Navidad',
                    txt:'Cervezas de Temporada Navideña.', 
                },
                {
                    id:'Verano',
                    txt:'Cervezas de Temporada Veraniega.',
                }
            ],
        }
    }

    render() {
        return (
            <div>
                <FilterBeers Beers={this.state.Beers} Buttons={this.state.Buttons}/>
            </div>
        )
    }
}
