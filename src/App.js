import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Layout from './Js/Components/Layout.jsx';
import Home from './Js/Components/Home.jsx'

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="reactjs_losreyes/" component={Home} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
