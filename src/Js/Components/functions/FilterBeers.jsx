import React, { useState, useMemo } from 'react'
import Equipo from '../../../Images/Equipo1.jpg'
import '../../../Css/Cervezas.css'

function useFilterBeers(Beers){
    var initialState = 'all'
    const [stateBtn, setStateBtn] = useState(initialState)
    const [ filteredBeer, setFilterBeer] = useState(Beers)
    useMemo(
        ()=>{
            const result = Beers.filter(Beer=>{
                return Beer.tem.includes(stateBtn)
            });
        setFilterBeer(result)
        }, [Beers, stateBtn]
    )
    return {filteredBeer, setStateBtn, stateBtn}
}


export default function Beers(props) {
    const Beers = props.Beers
    const Buttons = props.Buttons
    const { filteredBeer, setStateBtn, stateBtn } = useFilterBeers(Beers)
    return (
        <div>
            <section className="types-beers">
        {
            Buttons.map(Btn=>(
                <div className="btns-beers" id={Btn.id} onClick={()=>{
                    setStateBtn(Btn.id)
                    var item = document.getElementById(Btn.id)
                    if(item.classList.contains('btns-active')){
                        item.classList.remove('btns-active')
                    } else {
                        
                    }
                }}>
                    <h3 className="text">{Btn.txt}</h3>
                    <div className="line-h"></div>
                </div>
            ))
        }
            </section>
            <div className="beer-section-center">
                <h3 className="text-home">Seleccionaste: {stateBtn}</h3>
            </div>
            <div className="grid-beers">
                {
                    filteredBeer.map(
                        Beer => {
                            return <div>
                                <img src={Equipo} alt={Beer.name} className="beer-img" />
                                <h3 className="beer-title">{Beer.name}</h3>
                                <ul className="beer-text">
                                    <li>ALC: {Beer.alc}</li>
                                    <li>IBU: {Beer.ibu}</li>
                                    <li>Color: {Beer.color}</li>
                                    <li>Aroma: {Beer.fragance}</li>
                                    <li>Sabor: {Beer.taste}</li>
                                </ul>
                            </div>

                        }
                    )
                }
            </div>
        </div>
    )
}
